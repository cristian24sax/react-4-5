
import { useState } from 'react';
import styled from 'styled-components'

export const Container= styled.div<{visible:boolean}>`
    border-radius:50px;
    border:2px solid #fff;
    background-color:${props=> props.visible?'white':'transparent'};
    width:36px;
    height:35px;
    cursor: pointer;
`
export const Icon = styled.img<{visible:boolean}>`
    height:32px;
    width: 32px;
    object-fit:contain;
    border-radius:50px;
    margin-top:-2px;
    margin-left:-6px;
    visibility:${props=> props.visible?'visible':'hidden'};
`
export interface params {
    image:string;
    onClick?:(value:boolean)=>void;
}
export const Checkbox = (params:params):JSX.Element=> {

    const [visible, setVisible] = useState(false)
    const handleClick=()=>{
        setVisible(!visible)
        if(typeof params.onClick ==='function') params.onClick(visible)
    }
    return (
        <Container visible={visible} onClick={handleClick}>
            <Icon src={params.image} visible={visible} />
        </Container>
    )
}
