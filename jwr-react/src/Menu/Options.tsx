
import styled from 'styled-components'
import { Checkbox } from './Checkbox'
import check from '../img/check.jpg'
const Container = styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
    margin-left:15px;
`
const Title = styled.p`
    text-transform:uppercase;
    color:#fff;
    font-weight:bold;
    font-size:15px;

`


export interface params {
    value:string
    onClick?:(parmas:{value:string,state:boolean})=>void
};

export const Options = (params:params):JSX.Element => {
    const handleClick=(state:boolean)=>{
        if(typeof params.onClick ==='function'){
            params.onClick(
                {value:params.value, state}
            )
        }
    }
    return (
    <Container>
        <Title>
            {params.value} 
        </Title>
    <Checkbox image={check} onClick={handleClick}  />
</Container>
  )
  }
