
import './App.css'
import { Checkbox } from './Menu/Checkbox'
import check from './img/check.jpg'
import { Options,params } from './Menu/Options'
import styled from 'styled-components'
const Container = styled.p`
background-color:red;


`
function App() {
  const options:string[]=[
    '1up nutrition',
    'asitis',
    'avatar',
    'big muscles',
    'bpn sports',
    'bsn',
    'cellucor',
    'domin8r',
    'dymatize',
  ]
  const handleClick:params['onClick']= (e)=>{
    console.log(e)
  }
  return (
    <div className="App">
      <Container>
        {options.map((v,i)=><Options key={i} value={v} onClick={handleClick}/>)}
      </Container>
    </div>
  )
}

export default App
